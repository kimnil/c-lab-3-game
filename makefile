# The compiler to use
CC=g++

UNAME := $(shell uname)

# Compiler flags
# gcc and clang have different C11 flags
ifeq ($(UNAME), Linux)
    CFLAGS= -Wall -Wextra -std=c++0x -g
else
    CFLAGS= -Wall -Wextra -std=c++0x -Wc++11-extensions
endif

SRC=src
OBJ=obj
BIN=bin

# List of source files
SOURCES = $(wildcard $(SRC)/*.cpp)

# Assume there is an object file for each source file
OBJECTS = $(addprefix $(OBJ)/, $(notdir $(SOURCES:.cpp=.o)))

# Main target
EXECUTABLE=game

all: $(SOURCES) $(EXECUTABLE)

# Our target executable.
$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) -o $(BIN)/$@

# Every .cpp file has a corresponding object file.
$(OBJ)/%.o: $(SRC)/%.cpp
	$(CC) -c $(CFLAGS) $< -o $@

# Clean is phony because there's no file associated with the target.
.PHONY: clean
clean:
	- rm -rf obj/*.o $(EXECUTABLE)
	- rm -rf bin/*.d $(EXECUTABLE)
	- rm -rf bin/*.d-e $(EXECUTABLE)
	- rm -f $(EXECUTABLE)

# Include dependency files
-include $(SOURCES:%.cpp=%.d)
