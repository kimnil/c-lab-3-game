#include "actor.h"
#include "tile.h"
#include "game.h"
#include "item.h"
#include "combat.h"

#include "backpack.h"

using namespace lab3;
using namespace std;

// Default constructor. Note that id is auto increased in the initializers list.
Actor::Actor() : Container(20), id(nextId++) {
  pLocation = NULL;
}

// Destructor
Actor::~Actor() {

    cout << "The destructor of " << getName() << " was just called." << endl;
}

/**
 * Do some command with this player.
 */
void Actor::act(Command& command) {

  if (command.verb == Command::WHOAMI) {
    cout << "I am " << getName() << " the notorious " << getType() << "." << endl;
  }

  else if (command.verb == Command::GO) {
    string direction = command.getTarget();
    Tile * pTarget = this->getLocation()->getNeighbour(direction);

    // If there is no such direction - help the user.
    if (pTarget == NULL) {
      cout << "Oh dear... " << direction << " is not a valid direction. Did you mean one of these?" << endl << endl;
      cout << pLocation->getDirections() << endl << endl;
      cout << "Please try again." << endl;
    }

    if (pTarget) {

      bool isOpen = this->getLocation()->isExitOpen(direction);

      if (!isOpen) {
        cout << "Ouch! That door seem to be locked. Search for a lockpick!" << endl;
        return; 
      }

      if (this->move(*pTarget)) {
        pTarget->printFullDescription();
      } else {
        cout << "I can't seem to go there!" << endl;
      }
    }
  }

  else if (command.verb == Command::PICKUP) {
    string name = command.getTarget();

    Item * pTarget = this->getLocation()->getItem(name);

    if (pTarget) {
      this->pickUpItem(*pTarget);
    } else {
      cout << name << "... huh? I can't find no such item!" << endl;
    }
  }

  else if (command.verb == Command::ATTACK) {
    string target = command.getTarget();
    Actor * pTarget = pLocation->getActor(target);

    if (pTarget == this) {
      cout << "... huh? You can't attack yourself!" << endl;
      return;
    }

    if (pTarget) {
      Combat combat(this, pTarget);
      combat.begin();
    } else {
      cout << target << "... huh? I can't find no such being!" << endl;
    }
  }
  else if (command.verb == Command::LOOK) {
    this->getLocation()->printFullDescription();
  }

  else if (command.verb == Command::INV) {
    this->listItems();
  }

  else if (command.verb == Command::DROP) {
    string name = command.getTarget();

    Item * pTarget = this->getItem(name);

    if (pTarget) {
      this->removeItem(pTarget);
      this->getLocation()->addItem(pTarget);
    } else {
      cout << name << "... huh? I can't find no such item!" << endl;
    }
  }

  else if (command.verb == Command::MOVE) {
    string itemName = command.getTarget();
    string containerName = command.getTarget2();

    Container * pContainer = NULL;

    if (containerName == "self") {
      pContainer = dynamic_cast<Container *>(this);
    } else {
      Item * pItemContainer = this->getItem(containerName);
      pContainer = dynamic_cast<Container *>(pItemContainer);
    }

    Item * pItem = this->getItem(itemName);

    if (!pContainer) {
      cout << name << "... huh? I can't find no such container!" << endl;
      return;
    } 

    if (!pItem) {
      cout << name << "... huh? I can't find no such item!" << endl;
      return;
    }

    if (itemName == containerName) {
      cout << "... huh? What do you wish to accomplish by doing this?" << endl;
      return;
    }

    if(pContainer->canAddItem(pItem)) {
      this->removeItem(pItem);
      pContainer->addItem(pItem);
    } else {
      cout << pItem->getName() << " is to heavy for that container." << endl;
    }
  }

 else if (command.verb == Command::USE) {
    string name = command.getTarget();
    Item * pItem = this->getItem(name);
    
    if (pItem) {
      pItem->use(this); 
    } else {
      cout << name << "... huh? You don't have such item!" << endl;
    }
    
  }

  else {
    cout << "Command not implemented!" << endl;
  }

}

void Actor::setHitPoints(int new_hp) {
  if (new_hp > this->getStrength()) {
    this->hitPoints = this->getStrength();
  } else {
    this->hitPoints = new_hp;
  }

  if (this->getHitPoints() <= 0) {
    this->die();
  }
}

void Actor::die() {
  // Skriv ut vad som händer
  cout << this->getName() << " was killed.";
  if (this->getItems()->size() > 0) {
    cout << " All of his " << this->getItems()->size() << " items was dropped to the ground.";
  }
  cout << endl << endl;
  // Släng ut items på marken
  this->pLocation->getItems()->insert(this->pLocation->getItems()->end(), this->getItems()->begin(), this->getItems()->end());
  // Potientiell bugg här
  this->getItems()->clear();
  // The game loop will clean up dead players
  this->pLocation->removeActor(this);
}

/**
 * If possible, move actor to tile. This means that tile
 * has to have a connection with the actors current tile.
 * Just as you would expect.
 */
bool Actor::move(Tile& tile) {
  Tile * pCurrentTile;

  pCurrentTile = this->getLocation();

  // Actor has no position yet - ok, just put him in tile.
  if (pCurrentTile == NULL) {
    tile.addActor(this);
    this->setLocation(&tile);
    return true;
  }

  // Make sure tile is a neighbour to the current tile.
  for (Tile * pTile : pCurrentTile->getNeighbours()) {
    if (pTile == &tile) {
      // ok - found neighbour
      // do the move
      pCurrentTile->removeActor(this);
      tile.addActor(this);
      this->setLocation(&tile);
      return true;
    }
  }
  return false;
}

/**
 * If possible, move an item from a tile to the actors inventory.
 */
bool Actor::pickUpItem(Item& item) {
  Tile * pCurrentTile;

  pCurrentTile = this->getLocation();

  //Remove the item from the current tile
  if (this->addItem(&item)) {
    cout << getName() << " picked up " << item.getName() << endl;
    pCurrentTile->removeItem(&item);
    return true;
  } else {
    cout << getName() << " tried to pick up " << item.getName() << " but it was too heavy. Maybe another bag would help!" << endl;
    return false;
  }
}



// First character will get id 0. This will auto increase in Actor constructor list.
int Actor::nextId = 0;
