#ifndef ACTOR_H
#define ACTOR_H

#include <iostream>
#include <string>
#include <vector>
#include "command.h"
#include "container.h"

namespace lab3 {

  using namespace std;

  class Tile;
  class Game;
  class Item;

  class Actor : public Container {
  private:
    static int nextId; // Id counter
    string name;
    string type;
    int strength;
    int agility;
    int hitPoints;
    bool npc;
    const int id;
    Tile * pLocation;

    bool pickUpItem(Item& item);

    void die();

  public:

    // Getters
    int getStrength() const { return strength; };
    int getAgility() const { return agility; };
    int getHitPoints() const { return hitPoints; };
    string getName() const { return name; };
    string getType() const { return type; };
    int getId() const { return id; };
    bool getNpc() const { return npc; };
    Tile * getLocation() const { return pLocation; };
    bool isAlive() const { return this->getHitPoints() > 0; };

    // Setters
    void setStrength(int new_str) { strength = new_str; };
    void setAgility(int new_agi) { agility = new_agi; };
    void setHitPoints(int new_hp);
    void setName(string new_name) { name = new_name; };
    void setType(string new_type) { type = new_type; };
    void setNpc(bool new_npc) { npc = new_npc; };
    void setLocation(Tile * const pNewLocation) { pLocation = pNewLocation; };

    void act(Command& command);
    virtual int getDamageModifier() const = 0;

    bool move(Tile& tile);

    void printInventory(int numTabs = 0) const;

    // Comparison by actor id.
    bool operator==(const Actor & other) const { return other.getId() == this->getId(); }

    // Rule of three 
    // Constructor
    Actor();
    // Copy constructor
    Actor(const Actor & otherActor) = delete;
    // Destructor
    ~Actor();
  };

};

#endif
