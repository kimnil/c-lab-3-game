#include "apple.h"

#include <string>
#include <iostream>

#include "tile.h"

using namespace lab3;

// Rule of three 
// Constructor
Apple::Apple(int w, string name, string description) : Item(w, name, description) {
}

// Destructor
Apple::~Apple() {
    cout << " an apple was destroyed. " << endl;
}

/**
  * Using this lockpick automatically unlocks all the doors in your current room.
  */
void Apple::use(Actor * actor) {
  // Max out the actors health
  actor->setHitPoints(actor->getStrength());

  // Make the apple go away
  cout << actor->getName() << " feel completely refreshed after eating the apple." << endl;

  // Remove the apple from the actors inventory.
  actor->removeItem(this);

  // Free memory
  delete this;
}
