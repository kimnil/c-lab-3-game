#ifndef APPLE_H 
#define APPLE_H

#include <string>
#include "item.h"

namespace lab3 {

  class Apple: public Item {
  private:

  public:
    virtual void use(Actor *);
    
    // Rule of three 
    // Constructor
    Apple(int w, string name, string description);
    // Copy constructor
    Apple(const Apple& otherApple) = delete;
    // Destructor
    virtual ~Apple();
  };
}

#endif
