#include "backpack.h"
#include <string>
#include <iostream>

using namespace lab3;
using namespace std;

/**
  * Default backpack constructor.
  */
Backpack::Backpack(int w, string name, string description) : Container(25), Item(w, name, description) {
}

/**
  * Use the backpack, that is list its items.
  */
void Backpack::use(Actor * actor) {
  this->listItems();
}


Backpack::~Backpack() {
   cout << " backpack deconstructor " << endl; 
}
