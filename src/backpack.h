#ifndef BACKPACK_H
#define BACKPACK_H

#include <string>
#include "container.h"

namespace lab3 {
  class Backpack : public Container, public Item {
    private:

    public:
    virtual void use(Actor *);
    
    // Rule of three 
    // Constructor
    Backpack(int w, string name, string description);
    // Copy constructor
    Backpack(const Backpack & otherBackpack) = delete;
    // Destructor
    virtual ~Backpack(); 
  };
}

#endif
