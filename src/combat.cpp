#include "combat.h"
#include "tile.h"
#include "weapon.h"
#include <iostream>
#include <string>
#include <random>

using namespace lab3;
using namespace std;

/**
 * Constructor. Initialize a combat between two actors.
 */
Combat::Combat(Actor * const c1, Actor * const c2) : combatent1(c1), combatent2(c2) {

}

/**
 * Destructor
 */
Combat::~Combat() {
 // nothing to clean
}


/**
 * Let the fight begin! Take turns until someone escapes/dies/whatever.
 */
void Combat::begin() {
  bool combatent1Turn = true;

  cout << endl << combatent1->getName() << " entered a fight against " << combatent2->getName() << endl;
  cout << "---------------" << endl;

  // Initial attack
  attack(combatent1, combatent2);

  while(combatent1->isAlive() && combatent2->isAlive() && isInSameRoom()) {

    if (combatent1Turn) {
      takeTurn(combatent1, combatent2);
    } else {
      takeTurn(combatent2, combatent1);
    }

    // flip
    combatent1Turn = !combatent1Turn;
  }
}

void Combat::takeTurn(Actor * c1, Actor * c2) {
  if (c1->getNpc()) {
    if (c1->getHitPoints() < 30) {
      flee(c1, c2);
    } else {
      attack(c1, c2);
    }
  } 
  else {
    string buffer;
    cout << "Do you want to make another attack or flee?" << endl;
    std::getline(std::cin, buffer);

    if (buffer == "flee") {
      flee(c1, c2);
    } 
    else if (buffer == "attack") {
      attack(c1, c2);
    }
  }
}

void Combat::attack(Actor * c1, Actor * c2) {
  int c1Dmg;

  c1Dmg = c1->getDamageModifier() + rand() % 10;
  c2->setHitPoints(c2->getHitPoints() - c1Dmg);  
  cout << c1->getName() << " hit " << c2->getName() << " and it caused " << c1Dmg << " damage!" << endl;
  cout << "Now " << c2->getName() << " only has " << c2->getHitPoints() << " left." << endl << endl;
}

void Combat::flee(Actor * c1, Actor * c2) {
  int randNum = rand()%(100 + 1);

  // Let the dice decide if the flee is successfull
  if (randNum < c1->getAgility()) {
    int exitId = randNum % c1->getLocation()->getExits()->size();
    cout << c1->getName() << " managed to flee from the fight!" << endl;
    c1->move(* c1->getLocation()->getNeighbours().at(exitId));
  } else {
    cout << "Whilst fleeing " << c1->getName() << " received a fatal blow in the back by " << c2->getName() << "!" << endl;
    c1->setHitPoints(0);
  }
}

// Maybe this should be in the actor class instead?
bool Combat::isInSameRoom() const {
  return (combatent1->getLocation() == combatent2->getLocation());
}
