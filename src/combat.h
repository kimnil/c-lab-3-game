#ifndef COMBAT_H
#define COMBAT_H

#include "actor.h"

namespace lab3 {

  /**
   * This class represents a turn based combat system between two actors.
   */
  class Combat {


    private:
      Actor * const combatent1; // this is the initiaor
      Actor * const combatent2; // and this is the victim
      void takeTurn(Actor * const, Actor * const);
      void attack(Actor * const, Actor * const);
      void flee(Actor * const, Actor * const);
      bool isInSameRoom() const;

    public:

      Combat(Actor *, Actor *);
      ~Combat();
      Combat(const Combat& combat) = delete;
      void begin();

  };
}

#endif
