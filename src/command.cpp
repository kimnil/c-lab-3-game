#include "command.h"
#include <iostream>
#include <sstream>

using namespace lab3;
using namespace std;

Command::Command() {
}

Command::Command(string input) {
  this->raw = input;
}

/**
 * Read a line and return a new command.
 */
Command Command::readCommand() {
  string userInput;
  std::getline(std::cin, userInput);

  return Command(userInput);
}

/**
 * Read the input. Return false if its not a valid command.
 */
bool Command::parse() {

  string buff;
  stringstream ss(raw);

  ss >> buff;

  if (buff == "help") {
    verb = HELP;
    return true;
  } else if (buff == "quit") {
    verb = QUIT;
    return true;
  } else if (buff == "who") {
    verb = WHOAMI;
    return true;
  } else if (buff == "go") {
    verb = GO;
    // read direction to target1
    ss >> target1;
    return true;
  } else if (buff == "pickup") {
    verb = PICKUP;
    // read item to target1
    ss >> target1;
    return true;
  } else if (buff == "use") {
    verb = USE;
    // read item to target1
    ss >> target1;
    return true;
  } else if (buff == "attack") {
    verb = ATTACK;
    // read item to target1
    ss >> target1;
    return true;
  } else if (buff == "look") {
    verb = LOOK;
    return true;
  } else if (buff == "inv") {
    verb = INV;
    return true;
  } else if (buff == "drop") {
    verb = DROP;
    ss >> target1;
    return true;
  } else if (buff == "move") {
    verb = MOVE;
    ss >> target1;
    ss >> target2;
    return true;
  }

  return false;
}
