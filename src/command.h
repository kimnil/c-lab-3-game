#ifndef COMMAND_H
#define COMMAND_H

#include <string>

using namespace std;

namespace lab3 {
  class Command {
    private:
      string raw;
      string target1;
      string target2;

    public:
      // All the actions/verbs a player can perform
      enum Verb {HELP, GO, QUIT, WHOAMI, PICKUP, LOOK, ATTACK, INV, DROP, MOVE, USE};
      bool parse();
      enum Verb verb;

      Command();
      Command(string input);
      string getTarget() const { return target1; };
      string getTarget2() const { return target2; };

      static Command readCommand();
  };
}
#endif
