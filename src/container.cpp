#include "container.h"
#include "tile.h"
#include <iostream>
#include "main.h"

using namespace lab3;
using namespace std;

// Default constructor
Container::Container(int c) : capacity(c) {
  items = new std::vector<Item *>();
}

// Destructor
Container::~Container() {
    cout << "A container has been deconstructed. It conained " << items->size() << " items." << endl;

    deleteInVector(items);
}

/**
 * Remove an item from this container by ID matching.
 */
bool Container::removeItem(Item * pKeyItem) {
  Item * pItem = NULL;
  for (size_t i = 0; i < items->size(); i++) {
    pItem = items->at(i);

    if (*pKeyItem == *pItem) {
      items->erase(items->begin() + i);
      return true;
    }

    // If we have a container, do a recursive call
    Container * pContainer;
    if ((pContainer = dynamic_cast<Container *>(pItem)) != NULL) {
      bool removed = pContainer->removeItem(pKeyItem);

      // If there was a nested item, return.
      if (removed) {
        return true;
      }
    }
  }

  return false;
}

void Container::listItems(int numTabs) const {
  string response = "";
  string tabs = "";

  // build tabs
  for (int i = 0; i < numTabs; i++) {
    tabs += "\t";
  }

  // debug
  // cout << items->size() << endl;

  for (Item * item : *items) {

    // If we have a container, do a recursive call
    Container * pContainer;
    if ((pContainer = dynamic_cast<Container *>(item)) != NULL) {
      cout << tabs << item->getName() << ":" << endl;
      pContainer->listItems(++numTabs);
    } else {
      response += tabs;
      response += item->getName() + ", ";
    }

  }

  cout << response.substr(0, response.length() - 2) << endl;
}

/**
  * Add a new item to this container - if possible.
  */
bool Container::addItem(Item * pNewItem) {

  // cout << "currentWeight" << getCurrentWeight() << endl;
  // cout << "getCapacity" << getCapacity() << endl;
  // cout << "itemWeight" << pNewItem->getWeight() << endl;

  if(getCapacity() == 0 || getCurrentWeight() + pNewItem->getWeight() <= getCapacity()) {
    items->push_back(pNewItem);
    return true; 
  }

  return false;
}

/**
  * Check if its possible to add an item to a container
  */
bool Container::canAddItem(Item * pNewItem) const {

  if(getCapacity() == 0 || getCurrentWeight() + pNewItem->getWeight() <= getCapacity()) {
    return true; 
  }

  return false;
}

/*
  Return the capacity of this container 
  */
int Container::getCurrentWeight() const { 
  int currentWeight = 0;

  for (Item * item : *items) {
    // If we have a container, do a recursive call
    Container * pContainer;
    if ((pContainer = dynamic_cast<Container *>(item)) != NULL) {
      currentWeight += pContainer->getCurrentWeight();
      currentWeight += item->getWeight();
    } else {
      currentWeight += item->getWeight();
    }
  }

  return currentWeight;
};

/** 
 * Retrieves the item with a matching name.
 * or NULL if there is none.
 */
Item * Container::getItem(string name) const {
  for (Item * pItem : *items) {

    if (pItem->getName() == name) {
      return pItem;
    }

    // If we are searching a tiles container, don't search recursively
    /* TODO
    Tile const * pTile = dynamic_cast<Tile const *>(this);
    if (pTile != NULL) {
      continue;
    }
    */

    // If we have a container, do a recursive call
    Container * pContainer;
    if ((pContainer = dynamic_cast<Container *>(pItem)) != NULL) {
      Item * pNestedItem = pContainer->getItem(name);

      // If there was a nested item, return.
      if (pNestedItem) {
        return pNestedItem;
      }
    }
  }

  return NULL;
}


/**
 * Return a string representation of the items in the current tile
 * Example: Dagger, Sword
 */
string Container::getItemsString() const {
  string response = "";

  for (Item * item : *this->getItems()) {
    response += item->getName() + ", ";
  }

  return response.substr(0, response.length() - 2);
}
