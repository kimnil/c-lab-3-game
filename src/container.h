#ifndef CONTAINER_H
#define CONTAINER_H

#include <string>
#include "item.h"
#include <vector>

namespace lab3 {
  using namespace std;

  class Container {
  private:
    const int capacity;
    vector<Item *> * items;

  public:
    int getCapacity() const { return capacity; };
    int getCurrentWeight() const;
    vector<Item *> * getItems() const { return items; };
    Item * getItem(string name) const;
    
    bool addItem(Item * pNewItem);
    bool canAddItem(Item * pNewItem) const; 
    bool removeItem(Item * pItem);
    void listItems(int numTabs = 0) const;

    string getItemsString() const;


    // Rule of three 
    // Constructor
    Container(int capacity);
    // Copy constructor
    Container(const Container & otherContainer) = delete;
    // Destructor
    ~Container();
  };
}

#endif
