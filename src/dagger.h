#ifndef DAGGER_H
#define DAGGER_H

#include <string>
#include "weapon.h"

namespace lab3 {
  class Dagger : public Weapon {
  private:

  public:

    // Rule of three 
    // Constructor
    Dagger(int w, string name, string description);
    // Copy constructor
    Dagger(const Dagger & otherDagger) = delete;
    // Destructor
    virtual ~Dagger();
  };
}

#endif
