#include "game.h"
#include "dagger.h"
#include "sword.h"
#include "lockpick.h"
#include "apple.h"
#include "backpack.h"

#include <string>
#include <algorithm>

#include "main.h"

using namespace lab3;

/**
 * This is the main game loop where the user is prompted for action.
 */
void Game::play() {
  cout << "You dream..." << endl;
  cout << "------------" << endl;
  cout << "You are falling. Falling down from a tower so high only the clouds are visible down below. Despite your best attempts to grab hold of something on the walls you continue towards your immediate death. Just as your nose is about to touch the ground everything goes black..." << endl;
  cout << "Lying down on a wet and cold rocky floor you suddenly wake up. The only thing, despite your dream, that you can remember is that you are a warrior and that your name is Bedevere." << endl << endl;
  cout << "You stand up to your feet and start to look around..." << endl << endl;
  // Game loop
  while (1) {
    // Win game if wizard is dead
    if (isWizardDead()) {
      cout << "You win, good job Bedevere!" << endl;
      return;
    }

    // End game if all non-npc are dead
    if (!anyoneAlive()) {
      cout << "Game over!" << endl;
      return;
    }

    // Clean up dead players
    removeDeadActors();

    for (Actor * pActor : *pActors) {
      // Get command
      while(1) {
        if (pActor->getNpc()) {
          // TODO Automatisera här, typ alltid attack?
          break;
        }
        cout << "<id:" << pActor->getId() << (pActor->getNpc() ? "-NPC-" : "-Player-") << pActor->getType() << ">: ";
        command = Command::readCommand();
        if (command.parse()) {

          if (command.verb == Command::QUIT) {
            cout << "Bye!" << endl;
            return;
          }

          if (command.verb == Command::HELP) {
            cout << "Available commands are go, look, attack, use, inv, who, help" << endl;
            continue;
          }

          pActor->act(command);
          break; // next player

        } else {
          cout << "Uhm. Not sure what you mean. Type help if you are a lost!" << endl;
        }
      }
    }

    continue;
  }
}

/**
 * Create a new game instance.
 */
Game::Game() {
  srand(time(NULL)); // Seed the time

  // Initialize storage
  pMap = new std::vector<Tile *>();
  pActors = new std::vector<Actor *>();
  Command command;

  // Set up the map
  if (!initializeMap()) {
    std::cerr << "Couldn't initialize map, out of memory?" << std::endl;
    exit(1);
  }

  // Add actors
  if (!initializeActors()) {
    std::cerr << "Couldn't initialize actors, out of memory?" << std::endl;
    exit(1);
  }
}


/**
 * Given a map and an actor container, fill the container and place
 * the actors somehwere in the map.
 */
bool Game::initializeActors() {
  if (LOGLEVEL > 0) {
    cout << "Initializing actors" << endl;
  }
  Tile * pWarriorTile = pMap->at(0);
  Tile * pTrollTile = pMap->at(3);
  Tile * pGWTile = pMap->at(9);

  Item * pApple = new Apple(1, "apple", "a red apple");


  // Create a warrior
  Actor * pWarrior = new Warrior();
  pWarrior->setName("Bedevere");
  pWarrior->setNpc(false);
  pActors->push_back(pWarrior);
  pWarrior->move(*pWarriorTile);
  pWarrior->addItem(pApple);

  // Create a troll guard
  Actor * pTroll = new Troll();
  pTroll->setName("Troll");
  pTroll->setNpc(true);
  pActors->push_back(pTroll);
  pTroll->move(*pTrollTile);

  // Give the troll a mace and an apple
  Weapon * pTrollSword = new Sword(20, "longsword", "a heavy iron longsword");
  pTrollSword->setDamage(25);
  pTroll->addItem(pTrollSword);
  //pTroll->addItem(pApple);

  // Create a great wizard
  Actor * pGW = new Orc();
  pGW->setName("Wizard");
  pGW->setNpc(true);
  pActors->push_back(pGW);
  pGW->move(*pGWTile);

  // Give the wizard a staff
  Weapon * pGWKatana = new Sword(10, "katana", "a long sharp katana with leather grip");
  pGWKatana->setDamage(30);
  pGW->addItem(pGWKatana);

  if (LOGLEVEL > 2) {
    std::cout << "Done with actor initialization. Added " << pActors->size() << " actor(s)." << endl;
  }

  return true;
}

/**
 * Creates a map that is set up a few tiles and connect them. Returns true if all went well
 * otherwise false.
 */
bool Game::initializeMap() {

  if (LOGLEVEL > 0) {
    cout << "Initializing map" << endl;
  }

  // Create prison cell.
  Tile * pPrisonCell = new Indoor();
  if (pPrisonCell == NULL) { return false; }
  pPrisonCell->setName("A prison cell");
  pPrisonCell->setDescription("You are in a dark room with cavelike walls. There are a couple of barrels in the northwest corner and a door to the south. Through the door you can hear a distant sound of footsteps.");
  // TODO lägg till lockpick
  pMap->push_back(pPrisonCell);

  // Create the hallway
  Tile * pHallway= new Indoor();
  if (pHallway == NULL) { return false; }
  pHallway->setName("A dark hallway");
  pHallway->setDescription("You enter a dark passage with a single torch on the wall in front of you. To the east you can see a lit room and to the north you see the room where you woke up. West of you there is another room. It seems like the footsteps are coming from there.");
  pMap->push_back(pHallway);

  // Create the armory
  Tile * pArmory= new Indoor();
  if (pArmory == NULL) { return false; }
  pArmory->setName("The armory");
  pArmory->setDescription("You enter a room which looks like it could have been used as an armory of some sort. All around you there are weapons are armor of different sizes lying around. In one of the corners you see a weapons stand with a dagger and a sword that might actually be usable. You can also see a bag hanging on one of the walls.");
  // TODO lägg till bag, dagger, sword
  pMap->push_back(pArmory);

  // Create the cave entrance
  Tile * pCaveEntrance= new Indoor();
  if (pCaveEntrance == NULL) { return false; }
  pCaveEntrance->setName("Cave entrance");
  pCaveEntrance->setDescription("You find yourself in a narrow passway and you think you can hint the sunshine in southern end. There are footprints in the dusty ground from some kind of creature, a creature that certainly seems to be a lot bigger than yourself.");
  pMap->push_back(pCaveEntrance);

  // Create the forest
  Tile * pForest= new Outdoor();
  if (pForest == NULL) { return false; }
  pForest->setName("Forest");
  pForest->setDescription("A vast forest broadens out before you. All around you you can hear the sound of birds and the wind blowing in the trees. In the east you see a big hill that seems to be climbable and to the south the trees seems to be more coarse.");
  pMap->push_back(pForest);

  // Create the desert
  Tile * pDesert= new Outdoor();
  if (pDesert == NULL) { return false; }
  pDesert->setName("Desert");
  pDesert->setDescription("Ahead of you in every direction the desert seems to go on forever. With no water or protective trees in sight it seems like this is a dead end.");
  pMap->push_back(pDesert);

  // Create the hill
  Tile * pHill= new Outdoor();
  if (pHill == NULL) { return false; }
  pHill->setName("Hill");
  pHill->setDescription("Standing on top of the hill you can see clearly in each direction. To the north you can see a great mountain range and the entrance to the cave where you woke up. To the south the great desert stretches out in what seems like eternity. To the east you can see a great white tower, perhaps it’s the tower you saw in your dream. To the west you can see the forest from where you came, the slope downwards seems to be a bit too steep to climb back down however.");
  pMap->push_back(pHill);

  // Create the white tower
  Tile * pWhiteTower= new Indoor();
  if (pWhiteTower == NULL) { return false; }
  pWhiteTower->setName("The white tower");
  pWhiteTower->setDescription("You enter a great hall with monuments of dragons placed against the walls. All around you there are torches and inscriptions in a language that you can’t understand. There also seems to be a staircase going around the walls and you can hear loud murmurings from the top of the tower.");
  pMap->push_back(pWhiteTower);

  // Create the staircase
  Tile * pStaircase= new Indoor();
  if (pStaircase == NULL) { return false; }
  pStaircase->setName("Staircase");
  pStaircase->setDescription("You are walking along the staircase of the tower, at least what’s left of it. Far far down you can see the hall with the dragons and above you you can hear the murmuring even clearer.");
  pMap->push_back(pStaircase);

  // Create the great wizard's room
  Tile * pGreatWizardsRoom= new Indoor();
  if (pGreatWizardsRoom == NULL) { return false; }
  pGreatWizardsRoom->setName("Great wizard's room");
  pGreatWizardsRoom->setDescription("Taking the last step of the staircase you find yourself in a room filled with bottles and tables full of equipment. In the middle of the room there is a big throne made of a black rock like structure. On the top of the throne sits a creature, nothing like you’ve ever seen before.");
  pMap->push_back(pGreatWizardsRoom);

  // Connect the tiles
  pPrisonCell->addDirection("south", pHallway, false);

  pHallway->addDirection("north", pPrisonCell);
  pHallway->addDirection("west", pCaveEntrance);
  pHallway->addDirection("east", pArmory);

  pArmory->addDirection("west", pHallway);
  
  pCaveEntrance->addDirection("east", pHallway);
  pCaveEntrance->addDirection("south", pForest);

  pForest->addDirection("north", pCaveEntrance);
  pForest->addDirection("south", pDesert);
  pForest->addDirection("east", pHill);

  pDesert->addDirection("north", pForest);

  pHill->addDirection("east", pWhiteTower);

  pWhiteTower->addDirection("west", pHill);
  pWhiteTower->addDirection("up", pStaircase);

  pStaircase->addDirection("down", pWhiteTower);
  pStaircase->addDirection("up", pGreatWizardsRoom);

  pGreatWizardsRoom->addDirection("down", pStaircase);

  // Add items to tiles
  Weapon * pDagger = new Dagger(5, "dagger", "a small sharp dagger made of metal");
  Weapon * pSword = new Sword(15, "broadsword", "a massive two handed sword");
  Item * pLockpick = new Lockpick(1, "lockpick", "a well crafted lockpick made out of steel and the bone of a rat");
  Item * pBackpack = new Backpack(0, "backpack", "a simple leather container");

  pDagger->setDamage(15);
  pSword->setDamage(15);

  pArmory->addItem(pSword);
  pArmory->addItem(pDagger);
  pArmory->addItem(pBackpack);
  pPrisonCell->addItem(pLockpick);

  if (LOGLEVEL > 2) {
    std::cout << "Done with map initialization. Added " << pMap->size() << " room(s)." << endl;
  }

  return true;
}

/**
 * Removes an actor from the actors vector
 */
void Game::removeDeadActors() {
  for(auto it = pActors->begin(); it != pActors->end(); ) {

      //cout << "is " << (*it)->getName() << " dead?" << endl;

      // Delete actors if dead
      if ((*it)->isAlive() == false) {
        delete (*it);
        it = pActors->erase(it);
      } else {
        ++it;
      }
  }
  /*
  auto new_end = std::remove_if(pActors->begin(), pActors->end(),
                                [](const Actor * pActor) {
                                  return (!pActor->isAlive()); 
                               });

  for(auto it = new_end; it != pActors->end(); it++) {

    delete * it;
  }
  // Remove ALL matches
  pActors->erase(new_end, pActors->end());
  */
}

/**
 * All npn-NPC are dead, end game
 */
bool Game::anyoneAlive() {
  for(Actor * actor : *pActors) {
    if(!actor->getNpc() && actor->isAlive()) {
      return true;
    }
  }
  return false;
}

bool Game::isWizardDead() {
  for(Actor * actor : *pActors) {
    if(actor->getName() == "Wizard" && !actor->isAlive()) {
      return true;
    }
  }
  return false;  
}

/**
 * Deconstructor. Since we might have some stuff on the heap, be sure
 * to run the clean up method.
 */
Game::~Game() {
  cleanUp();
}

/**
 * Free memory.
 */
void Game::cleanUp() {
    deleteInVector(pMap);
    deleteInVector(pActors);
}
