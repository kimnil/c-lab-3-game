#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <random>

#include "tile.h"
#include "actor.h"
#include "indoor.h"
#include "outdoor.h"
#include "command.h"

#include "warrior.h"
#include "rogue.h"
#include "troll.h"
#include "orc.h"

#define LOGLEVEL 3

namespace lab3 {

  class Game {
  private:
    // This is where we are our tiles, their connections and our actors.
    std::vector<Tile *> * pMap;
    std::vector<Actor *> * pActors;
    Command command;

    void cleanUp();
    bool initializeActors();
    bool initializeMap();
    void removeDeadActors();
    bool isWizardDead();
    bool anyoneAlive();

  public:
    void play();
    Game();
    ~Game();
    Game(const Game & otherGame) = delete;

    // Note: Should we move these functions to actor? to tile? to item?

  };
}

#endif
