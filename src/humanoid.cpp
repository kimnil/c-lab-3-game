#include "humanoid.h"
#include <string>

using namespace lab3;

// Implementations
//
Humanoid::Humanoid() {
}

// Destructor
Humanoid::~Humanoid() {

}

/**
 * By looking through the humnaoids weapons - returns an integer given the humanoid a bonus.
 * */
int Humanoid::getDamageModifier() const {
  Weapon * pWeapon = NULL;

  for (Item * pItem : *this->getItems()) {
    pWeapon = dynamic_cast<Weapon *>(pItem);
    if (pWeapon != NULL) {
      cout << this->getName() << " will use " << pItem->getName() << " in the fight." << endl;
      return (pWeapon->getDamage() + this->getAgility() / 10);
      break;
    }
  }
  cout << this->getName() << " will use his mighty fists in the fight." << endl;
  return 0;
}
