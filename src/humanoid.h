#ifndef HUMANOID_H
#define HUMANOID_H

#include "actor.h"
#include "weapon.h"

namespace lab3 {

  class Actor;
  class Humanoid : public Actor {

    protected:
    bool sex; // MALE = false

    public:
    virtual int getDamageModifier() const;

    bool isFemale() const { return sex; };
    bool isMale() const { return !sex; };
    void setSex(bool new_sex) { sex = new_sex; };

    // Rule of three 
    // Constructor
    Humanoid();
    // Copy constructor
    Humanoid(const Humanoid& otherHumanoid) = delete;
    // Destructor
    ~Humanoid();


  };

}

#endif
