#include "item.h"
#include <string>
#include <iostream>

using namespace lab3;
using namespace std;

// Rule of three 
// Constructor
Item::Item(int w, string n, string d) : weight(w), name(n), description(d), id(nextId++) {

}

// Destructor
Item::~Item() {
    cout << getName() << " was destroyed." << endl;
}

int Item::nextId = 0;
