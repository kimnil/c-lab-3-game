#ifndef ITEM_H
#define ITEM_H

#include <string>

namespace lab3 {
	using namespace std;
  class Actor;

  class Item {
  private:
    const int weight;
    static int nextId;
    const string name;
    const string description;
    const int id;
    
  public:
    string getDescription() const { return description; };
    string getName() const { return name; };
    int getWeight() const { return weight; };
    int getId() const { return id; };

    // Comparison by actor id.
    bool operator==(const Item & other) const { return other.getId() == this->getId(); }

    // All items can by used by an actor.
    virtual void use(Actor *) = 0;

    // Rule of three 
    // Constructor
    Item(int w, string name, string description);
    // Copy constructor
    Item(const Item & otherItem) = delete;
    // Destructor
    virtual ~Item() = 0;
  };
}

#endif
