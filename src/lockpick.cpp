#include "lockpick.h"

#include <string>
#include <iostream>

#include "tile.h"

using namespace lab3;

// Rule of three 
// Constructor
Lockpick::Lockpick(int w, string name, string description) : Item(w, name, description) {
}

// Destructor
Lockpick::~Lockpick() {

}

/**
  * Using this lockpick automatically unlocks all the doors in your current room.
  */
void Lockpick::use(Actor * actor) {
  Tile * playerTile = actor->getLocation();
  playerTile->openAllExits();
}