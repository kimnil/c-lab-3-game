#ifndef LOCKPICK_H
#define LOCKPICK_H

#include <string>
#include "item.h"

namespace lab3 {

  class Lockpick : public Item {
  private:

  public:
    virtual void use(Actor *);
    
    // Rule of three 
    // Constructor
    Lockpick(int w, string name, string description);
    // Copy constructor
    Lockpick(const Lockpick & otherLockpick) = delete;
    // Destructor
    ~Lockpick();
  };
}

#endif
