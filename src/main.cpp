#include <iostream>
#include <vector>

#include "game.h"
#include "main.h"

using namespace lab3;
using namespace std;


// This is the games entry point.
int main() {
  std::cout << "Welcome to lab 3 game!" << std::endl;

  Game game;
  game.play();

  return 0;
}

