#ifndef MAIN_H
#define MAIN_H

#include <vector>
#include <iostream>

using namespace std;
using namespace lab3;

template <class T>
void deleteInVector(vector<T*>* deleteme) {

    cout << "deleting vector " << endl;

    while(!deleteme->empty()) {
        delete deleteme->back();
        deleteme->pop_back();
    }

    delete deleteme;
}

#endif

