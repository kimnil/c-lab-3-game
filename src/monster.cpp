#include "monster.h"
#include "weapon.h"

using namespace lab3;
// Implementations
//
Monster::Monster() {
}

Monster::~Monster() {
}

/**
 * By looking through the humnaoids weapons - returns an integer given the humanoid a bonus.
 * */
int Monster::getDamageModifier() const {
  Weapon * pWeapon = NULL;

  for (Item * pItem : *this->getItems()) {
    pWeapon = dynamic_cast<Weapon *>(pItem);
    if (pWeapon != NULL) {
      cout << this->getName() << " will use " << pItem->getName() << " in the fight." << endl;
      return (pWeapon->getDamage() + this->getStrength() / 10);
      break;
    }
  }
  cout << this->getName() << " will use his claws in the fight." << endl;
  return 0;
}
