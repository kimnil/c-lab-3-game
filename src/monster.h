#ifndef MONSTER_H
#define MONSTER_H

#include "actor.h"

namespace lab3 {

  class Monster : public Actor {

    public:
    virtual int getDamageModifier() const;

    // Rule of three
    // Constructor
    Monster();
    // Copy constructor
    Monster(const Monster& otherMonster) = delete;
    // Destructor
    ~Monster();

  };

}

#endif
