#include "orc.h"

using namespace lab3;

Orc::Orc() {
  setType("orc");
  setAgility(40);
  setStrength(60);
  setHitPoints(130);
}

Orc::~Orc() {
}
