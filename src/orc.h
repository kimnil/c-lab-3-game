#ifndef ORC_H
#define ORC_H

#include "monster.h"

namespace lab3 {
  class Orc : public Monster {
    public:
    // Rule of three
    // Constructor
    Orc();
    // Copy constructor
    Orc(const Orc& otherOrc) = delete;
    // Destructor
    ~Orc();

  };
}

#endif
