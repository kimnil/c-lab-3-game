#include "rogue.h"

using namespace lab3;
// Implementations

Rogue::Rogue() {
  setType("rogue");
  setStrength(75);
  setHitPoints(75);
  setAgility(75);
}
