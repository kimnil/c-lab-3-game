#ifndef ROGUE_H 
#define ROGUE_H

#include "humanoid.h"

namespace lab3 {
  class Rogue : public Humanoid {

  public:
    // Rule of three
    // Constructor
    Rogue();
    // Copy constructor
    Rogue(const Rogue& otherRogue);
    // Destructor
    ~Rogue();

  };
}

#endif
