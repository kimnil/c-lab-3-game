#ifndef SWORD_H
#define SWORD_H

#include <string>
#include "weapon.h"

namespace lab3 {
  class Sword : public Weapon {
  private:

  public:
    // Rule of three 
    // Constructor
    Sword(int w, string name, string description);
    // Copy constructor
    Sword(const Sword & otherSword) = delete;
    // Destructor
    virtual ~Sword();
  };
}

#endif
