#include "tile.h"
#include "item.h"

#include <algorithm>

using namespace lab3;
using namespace std;

/** 
 * Retrieves the tile in direction, eg north.
 * or NULL if there is none.
 */
Tile * Tile::getNeighbour(string direction) const {
  for (Exit exit : exits) {
    if (exit.name == direction) {
      return exit.pNeighbour;
    }
  }

  return NULL;
}

/**
  * Returns an exit by name or false if there is no such exit.
  */
bool Tile::isExitOpen(string name) const {
  for (Exit exit : exits) {
    if (exit.name == name) {
      return exit.open;
    }
  }

  return false;
}

/**
  * Opens all doors in this room.
  */
void Tile::openAllExits() {
  for (auto i = 0; i < exits.size(); i++) {
     Exit& exit = exits[i];
     exit.open = true;
  }
}

/** 
 * Retrieves all the tiles connected to the current one
 */
vector<Tile *> Tile::getNeighbours() const {
  vector<Tile *> neighbours = vector<Tile *>();

  for (Exit exit : exits) {
    neighbours.push_back(exit.pNeighbour);
  }

  return neighbours;
}

/** 
 * Prints the full description, including title and exits
 */
void Tile::printFullDescription() const {
  cout << endl << this->getName() << endl;
  for(int i = 0; i < (int)this->getName().length(); i++) {
    cout << "-";
  }
  cout << endl << this->getDescription() << endl << endl;
  cout << "Creatures: " << this->getActorsString() << endl;
  cout << "Items: " << this->getItemsString() << endl;
  cout << "Exists: " << this->getDirections() << endl << endl;
}

/**
 * Retrieves the actor with a matching name.
 * or NULL if there is none.
 */
Actor * Tile::getActor(string name) const {
  for (Actor * pActor: *actors) {
    if (pActor->getName() == name) {
      return pActor;
    }
  }

  return NULL;
}

/**
 * Remove an actor from this tile by ID matching.
 */
void Tile::removeActor(Actor * pActor) {
  int keyId = pActor->getId();

  // Finds an element and moves it to the end of the list. This isn't really efficient
  // because it will always iterate the entire list instead of stopping after finding
  // the actor we are looking for. But its an example usage of lambda expressions. And its
  // ugly as hell!
  auto new_end = std::remove_if(actors->begin(), actors->end(),
                                [&keyId](const Actor * pActor) {
                                  return pActor->getId() == keyId; 
                               });

  // Remove ALL matches
  actors->erase(new_end, actors->end());
}



/**
 * Update the current name.
 */
void Tile::setName(string newName) {
  this->name= newName;
}

/**
 * Update the current description.
 */
void Tile::setDescription(string newDescription) {
  this->description = newDescription;
}

/**
 * Return a string representation of the possible direction.
 * Example: north, south, east
 */
string Tile::getDirections() const {
  string response = "";

  for (Exit exit : this->exits) {
    response += exit.name + ", "; // + " would take you to " + exit.pNeighbour->getName() + "\n";
  }

  return response.substr(0, response.length() - 2);
}

/**
 * Return a string representation of the actors in the current tile
 * Example: A troll guard, Sir Bedevere
 */
string Tile::getActorsString() const {
  string response = "";

  for (Actor * actor : *this->actors) {
    response += actor->getName() + ", ";
  }

  return response.substr(0, response.length() - 2);
}


void Tile::addDirection(string direction, Tile * pNeighbour, bool open) {
  Exit newExit = Exit();
  newExit.name = direction;
  newExit.pNeighbour = pNeighbour;
  newExit.open = open;

  this->exits.push_back(newExit);
}

/**
 * Default constructor
 */
Tile::Tile() : Container(0) {
  actors = new vector<Actor *>();
}

/**
 * Destructor
 */
Tile::~Tile() {
   actors->clear();
   delete actors;
}
