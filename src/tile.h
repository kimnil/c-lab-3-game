#ifndef TILE_H
#define TILE_H

#include "actor.h"
#include "container.h"

#include <string>
#include <vector>

namespace lab3 {

  using namespace std;

  class Actor; // Annoying forward declaration. Necessary?

  class Tile : public Container {
    private:
      // Internal struct for exits.
      struct Exit {
        string name; // north, south etc.
        Tile * pNeighbour;
        bool open;

        // Compare exits by their string name.
        bool operator==(const Exit& otherExit) {
          return name == otherExit.name;
        }
      };

      string name;
      string description;
      vector<Exit> exits;

      // Pointers to the actors that are on this tile
      vector<Actor *> * actors;

    public:
      void addActor(Actor * pNewActor) { actors->push_back(pNewActor); };
      void removeActor(Actor * pActor);
      void setName(string);
      void setDescription(string);
      void addDirection(string, Tile *, bool = true);

      Tile * getNeighbour(string direction) const;
      Actor * getActor(string name) const;
      string getName() const { return this->name; };
      string getDescription() const { return this->description; };
      void printFullDescription() const;
      string getDirections() const;
      int getNumActors() const { return actors->size(); };
      string getActorsString() const;
      vector<Exit> * getExits() const { return const_cast<vector<Exit> * >(&exits); };
      vector<Tile *> getNeighbours() const;
      bool isExitOpen(string name) const;
      void openAllExits();

      // Rule-of-three operators
      Tile();
      Tile(Tile const & otherTile) = delete;
      ~Tile();
  };
}

#endif
