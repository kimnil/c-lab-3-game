#include "troll.h"

using namespace lab3;

Troll::Troll() {
  setType("troll");
  setAgility(20);
  setStrength(100);
  setHitPoints(130);
}

Troll::~Troll() {
}