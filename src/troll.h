#ifndef TROLL_H
#define TROLL_H

#include "monster.h"

namespace lab3 {
  class Troll : public Monster {
    public:
    // Rule of three
    // Constructor
    Troll();
    // Copy constructor
    Troll(const Troll& otherTroll) = delete;
    // Destructor
    ~Troll();

  };
}

#endif
