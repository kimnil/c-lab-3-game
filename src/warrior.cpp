#include "warrior.h"
#include <string>
#include <iostream>

using namespace lab3;
using namespace std;
// Implementations
//
Warrior::Warrior() {
  setType("warrior");
  setAgility(40);
  setStrength(100);
  setHitPoints(100);
}

Warrior::~Warrior() {
}
