#ifndef WARRIOR_H
#define WARRIOR_H

#include "humanoid.h"

namespace lab3 {
  class Humanoid;
  class Warrior : public Humanoid {

    public:
    // Rule of three
    // Constructor
    Warrior();
    // Copy constructor
    Warrior(const Warrior& otherWarrior) = delete;
    // Destructor
    ~Warrior();
  };
}

#endif
