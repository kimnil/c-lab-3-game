#include "weapon.h"
#include <string>
#include <iostream>

using namespace std;
using namespace lab3;

// Rule of three 
// Constructor
Weapon::Weapon(int w, string name, string description) : Item(w, name, description) {
}

// Destructor
Weapon::~Weapon() {

}

/**
  * Cant directly use a weapon.
  */
void Weapon::use(Actor * actor) {
  cout << "You will automatically use your weapon when attacking!" << endl;
}