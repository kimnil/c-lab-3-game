#ifndef WEAPON_H
#define WEAPON_H

#include <string>
#include "item.h"

namespace lab3 {

  class Actor;
  
  class Weapon : public Item {
  private:
    int damage;

  public:
    virtual void use(Actor *);
    int getDamage() const { return damage; };

    void setDamage(int new_damage) { damage = new_damage; };

    // Rule of three 
    // Constructor
    Weapon(int w, string name, string description);
    // Copy constructor
    Weapon(const Weapon & otherWeapon);
    // Destructor
    virtual ~Weapon();
  };
}

#endif
